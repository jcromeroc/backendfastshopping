const Product = require('../models/Product');
const Customer = require('../models/Customer');
const Order = require('../models/Order');
const OrderSummary = require('../models/OrderSummary');

exports.GetAllProducts = async (req, res) => {
    await Product.findAll()
    .then((result) => {
        res.json({
            code: "200",
            data: result
        });
    })
    .catch((error) => {
        res.json({
            code: "404",
            data: "Products not found",
            error: error
        });   
    });
};

exports.GetProductsPerPage = async (req, res) => {
    var perPage = 20;
    var page = parseInt(req.params.page) || 1;
    var skip = (page-1) * perPage; 

    var order = req.params.order || "AlphaOrder";
    var query = ['Title', 'ASC'];

    if (order == "AlphaOrder") {
        query = ['Title', 'ASC'];
    } else if (order == "LowestPrice") {
        query = ['Price', 'ASC'];
    } else if (order == "MostRecent") {
        query = ['Date', 'DESC'];
    }

    await Product.findAndCountAll({
        order: [query],
        offset: skip,
        limit: perPage
    })
    .then((result) => {
        var numRows = result.count;
        var numPages = Math.ceil(numRows / perPage);
        res.json({
            code: "200",
            data: result.rows,
            perPage,
            numPages,
            page,
            total: numRows
        });
    })
    .catch((error) => {
        res.json({
            code: "404",
            data: "Products not found",
            error: error
        });   
    });
};

exports.GetCustomer =  async (req, res) => {
    const { email } = req.params;

    await Customer.findOne({
        where: {
            Email: email
        }
    })
    .then((result) => {
        if (result != null) {
            res.json({
                code: "200",
                data: result
            });
        } else {
            res.json({
                code: "404",
                data: "Customer not found",
                error: error
            });
        }
    })
    .catch((error) => {
        res.json({
            code: "404",
            data: "Customer not found",
            error: error
        });   
    });
};

exports.PostCustomer = async (req, res) => {
    const { Id, Full_name, Address, Phone_number, Email } = req.body;

    await Customer.create({
        Id,
        Full_name,
        Address,
        Phone_number,
        Email
    })
    .then((result) => {
        res.json({
            code: "200",
            data: result,
            id: result.Id
        });
    })
    .catch((error) => {
        res.json({
            code: "400",
            data: "Customer not added",
            error: error
        });   
    });
};

exports.PostOrder = async (req, res) => {
    const { Date, Total, Customer_Id } = req.body;

    await Order.create({
        Date,
        Total,
        Customer_Id,
    })
    .then((result) => {
        res.json({
            code: "200",
            data: "Added Order",
            id: result.Id
        });
    })
    .catch((error) => {
        res.json({
            code: "400",
            data: "Order not added",
            error: error
        });   
    });
};

exports.UpdateOrder = async (req, res) => {
    const { Id, Total } = req.body;

    await Order.update({
        Total
    },{
        where: {
            Id: Id
        }
    })
    .then((result) => {
        res.json({
            code: "200",
            data: "Update Order"
        });
    })
    .catch((error) => {
        res.json({
            code: "400",
            data: "Order not update",
            error: error
        });   
    });
};

exports.PostOrderSummary = async (req, res) => {
    const { Units, Total_price, Order_Id, Product_Id } = req.body;

    await OrderSummary.create({
        Units,
        Total_price,
        Order_Id,
        Product_Id
    })
    .then((result) => {
        res.json({
            code: "200",
            data: "Added OrderSummary"
        });
    })
    .catch((error) => {
        res.json({
            code: "400",
            data: "OrderSummary not added",
            error: error
        });   
    });
};