SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema shopping_cart
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `shopping_cart` ;

-- -----------------------------------------------------
-- Schema shopping_cart
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `shopping_cart` DEFAULT CHARACTER SET utf8 ;
USE `shopping_cart` ;

-- -----------------------------------------------------
-- Table `shopping_cart`.`Product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shopping_cart`.`Product` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Title` VARCHAR(100) NOT NULL,
  `Category` VARCHAR(100) NOT NULL,
  `Description` VARCHAR(200) NOT NULL,
  `Price` FLOAT NOT NULL,
  `Image` VARCHAR(100) NULL,
  `Date` DATETIME NOT NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shopping_cart`.`Customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shopping_cart`.`Customer` (
  `Id` INT NOT NULL,
  `Full_name` VARCHAR(50) NOT NULL,
  `Address` VARCHAR(100) NOT NULL,
  `Phone_number` VARCHAR(20) NOT NULL,
  `Email` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `Email_UNIQUE` (`Email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shopping_cart`.`Order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shopping_cart`.`Order` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Date` DATETIME NULL,
  `Total` FLOAT NOT NULL,
  `Customer_Id` INT NOT NULL,
  PRIMARY KEY (`Id`, `Customer_Id`),
  INDEX `fk_Order_Customer1_idx` (`Customer_Id` ASC),
  CONSTRAINT `fk_Order_Customer1`
    FOREIGN KEY (`Customer_Id`)
    REFERENCES `shopping_cart`.`Customer` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shopping_cart`.`OrderSummary`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shopping_cart`.`OrderSummary` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Units` INT NOT NULL,
  `Total_price` FLOAT NOT NULL,
  `Order_Id` INT NOT NULL,
  `Product_Id` INT NOT NULL,
  PRIMARY KEY (`Id`, `Order_Id`, `Product_Id`),
  INDEX `fk_OrderSummary_Order1_idx` (`Order_Id` ASC),
  INDEX `fk_OrderSummary_Product1_idx` (`Product_Id` ASC),
  CONSTRAINT `fk_OrderSummary_Order1`
    FOREIGN KEY (`Order_Id`)
    REFERENCES `shopping_cart`.`Order` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_OrderSummary_Product1`
    FOREIGN KEY (`Product_Id`)
    REFERENCES `shopping_cart`.`Product` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


-- -----------------------------------------------------
-- Insert Products
-- -----------------------------------------------------
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Zapatos Adidas', 'productos,zapatos,ropa', 'Maravillosos zapatos adidas', 25, 'zapatos.png', NOW());
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'MacBookPro', 'tecnologia,laptop', 'Pontente laptop del 2020', 1500, 'mac.png', NOW());
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Chaqueta Puma', 'productos,ropa,chaqueta', 'Chaqueta muy fina de buena calidad', 40.5, 'chaqueta.png', NOW() - INTERVAL 1 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Router TP Lnk', 'tecnologia,router', 'Router con gran alcance', 45.9, 'router.png', NOW() - INTERVAL 2 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'PC de escritorio', 'tecnologia,PC', 'Poderoso CPU con caracteristicas muy actuales', 2000, 'pc.png', NOW() - INTERVAL 3 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Play Station 3', 'tecnologia,Consola', 'Tercera consola de sobremesa fabricada por Sony Computer Entertainment', 100, 'play3.png', NOW() - INTERVAL 1 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Play Station 2', 'tecnologia,Consola', 'Única que ha tenido un ciclo de vida tan largo que ha logrado competir con las de una generación siguiente', 90, 'play2.png', NOW() - INTERVAL 4 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'xboox3600', 'tecnologia,Consola', ' Fue producida por Microsoft y desarrollada en colaboración con IBM y ATI.', 110, 'xboox360.png', NOW() - INTERVAL 2 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Wii', 'tecnologia,Consola', ' Consola producida por Nintendo que se caracteriza por su mando inalámbrico', 70, 'wii.png', NOW() - INTERVAL 1 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'PSP', 'tecnologia,Consola', 'Capaces de cumplir las funciones de una consola de sobremesa, un ordenador y un smartphone', 120, 'psp.png', NOW() - INTERVAL 3 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Nintendo DS', 'tecnologia,Consola', 'Consola portátil que lanzó Nintendo al mercado en el 2004.', 100, 'nds.png', NOW() - INTERVAL 6 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'PlayStation Vita', 'tecnologia,Consola', 'Permite a los usuarios conectarse a través de redes de telefonía celular y Wifi.', 170, 'pspvita.png', NOW() - INTERVAL 4 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Play Station', 'tecnologia,Consola', 'Primera en emplear el CD-ROM como soporte de almacenamiento', 55, 'play1.png', NOW() - INTERVAL 3 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Nintendo Gamecube', 'tecnologia,Consola', ' Sucesora del Nintendo 64, el Gamecube. El GameCube Optical Disc, como formato de almacenamiento.', 65, 'gamecube.png', NOW() - INTERVAL 1 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Nintendo 64', 'tecnologia,Consola', ' Esta consola fue bautizada así por su procesador principal de 64 bits', 45, 'nintendo64.png', NOW() - INTERVAL 7 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Zapatos Puma', 'productos,zapatos,ropa', 'Maravillosos zapatos Pumas', 35, 'zapatopuma.png', NOW());
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Zapatos Converse', 'productos,zapatos,ropa', 'Maravillosos zapatos converse', 30, 'zapatoconverse.png', NOW());
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Zapatos Vans', 'productos,zapatos,ropa', 'Maravillosos zapatos vans', 25, 'zapatovans.png', NOW());
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Chaqueta Converse', 'productos,ropa,chaqueta', 'Chaqueta muy fina de buena calidad', 51.5, 'chaquetaconverse.png', NOW() - INTERVAL 4 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Chaqueta QuikSilver', 'productos,ropa,chaqueta', 'Chaqueta muy fina de buena calidad', 45.9, 'chaquetaquik.png', NOW() - INTERVAL 1 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Chaqueta Nike', 'productos,ropa,chaqueta', 'Chaqueta muy fina de buena calidad', 65.5, 'chaquetanike.png', NOW() - INTERVAL 6 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Reloj Patek Philippe', 'productos,ropa,accesorio', 'Reloj de lujo de gran calidad', 4180, 'reloj1.png', NOW() - INTERVAL 5 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Reloj Vacheron Constantin', 'productos,ropa,accesorio', 'Reloj de lujo de gran calidad', 5000, 'reloj2.png', NOW() - INTERVAL 8 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Reloj Audemars Piguet', 'productos,ropa,accesorio', 'Reloj de lujo de gran calidad', 5150, 'reloj3.png', NOW() - INTERVAL 4 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Reloj Girard-Perregaux', 'productos,ropa,accesorio', 'Reloj de lujo de gran calidad', 7250, 'reloj4.png', NOW() - INTERVAL 3 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Cartera Chanel', 'productos,cartera,accesorio', 'Maravillosas carteras de gran calidad y renombre', 2500, 'cartera1.png', NOW() - INTERVAL 7 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Cartera Prada', 'productos,cartera,accesorio', 'Maravillosas carteras de gran calidad y renombre', 2750, 'cartera2.png', NOW() - INTERVAL 2 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Cartera Fendi', 'productos,cartera,accesorio', 'Maravillosas carteras de gran calidad y renombre', 2480, 'cartera3.png', NOW() - INTERVAL 1 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Cartera Dior', 'productos,cartera,accesorio', 'Maravillosas carteras de gran calidad y renombre', 2780, 'cartera4.png', NOW() - INTERVAL 6 DAY);
INSERT INTO `Product` (`Id`, `Title`, `Category`, `Description`, `Price`, `Image`, `Date`) VALUES (NULL, 'Cartera Gucci', 'productos,cartera,accesorio', 'Maravillosas carteras de gran calidad y renombre', 3500, 'cartera5.png', NOW() - INTERVAL 4 DAY);

