const { DataTypes, Model } = require('sequelize');
const sequelize = require('../config/db-connection');

class Customer extends Model {}

Customer.init({
  // Model attributes are defined here
  Id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    allowNull: false
  },
  Full_name: {
    type: DataTypes.STRING(50),
    allowNull: false
  },
  Address: {
    type: DataTypes.STRING(100),
    allowNull: false
  },
  Phone_number: {
    type: DataTypes.STRING(20),
    allowNull: false
  },
  Email : {
    type: DataTypes.STRING(45),
    allowNull: false
  }
}, {
    // Other model options go here
    sequelize, // We need to pass the connection instance
    modelName: 'Customer', // We need to choose the model name
    freezeTableName: true,
    timestamps: false,
});

module.exports = Customer;