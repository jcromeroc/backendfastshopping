const { DataTypes, Model } = require('sequelize');
const sequelize = require('../config/db-connection');

class Product extends Model {}

Product.init({
  // Model attributes are defined here
  Id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  Title: {
    type: DataTypes.STRING(100),
    allowNull: false
  },
  Category: {
    type: DataTypes.STRING(100),
    allowNull: false
  },
  Description: {
    type: DataTypes.STRING(200),
    allowNull: false
  },
  Price: {
    type: DataTypes.FLOAT,
    allowNull: false
  },
  Image: {
    type: DataTypes.STRING(100),
    allowNull: true
  },
  Date: {
    type: DataTypes.DATE,
    allowNull: false
  }
}, {
    // Other model options go here
    sequelize, // We need to pass the connection instance
    modelName: 'Product', // We need to choose the model name
    freezeTableName: true,
    timestamps: false,
});

module.exports = Product;