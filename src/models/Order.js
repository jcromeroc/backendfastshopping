const { DataTypes, Model } = require('sequelize');
const sequelize = require('../config/db-connection');

class Order extends Model {}

Order.init({
  // Model attributes are defined here
  Id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true
  },
  Date: {
    type: DataTypes.DATE,
    allowNull: false
  },
  Total: {
    type: DataTypes.FLOAT,
    allowNull: false
  },
  Customer_Id: {
    type: DataTypes.INTEGER,
    references: 'Customer', // <<< Note, its table's name, not object name
    referencesKey: 'Id', // <<< Note, its a column name
    allowNull: false 
  }
}, {
    // Other model options go here
    sequelize, // We need to pass the connection instance
    modelName: 'Order', // We need to choose the model name
    freezeTableName: true,
    timestamps: false,
});

module.exports = Order;