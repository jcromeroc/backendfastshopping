const { DataTypes, Model } = require('sequelize');
const sequelize = require('../config/db-connection');

class OrderSummary extends Model {}

OrderSummary.init({
  // Model attributes are defined here
  Id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true
  },
  Units: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  Total_price: {
    type: DataTypes.FLOAT,
    allowNull: false
  },
  Order_Id : {
    type: DataTypes.INTEGER,
    references: 'Order', // <<< Note, its table's name, not object name
    referencesKey: 'Id', // <<< Note, its a column name
    allowNull: false 
  },
  Product_Id : {
    type: DataTypes.INTEGER,
    references: 'Product', // <<< Note, its table's name, not object name
    referencesKey: 'Id', // <<< Note, its a column name
    allowNull: false 
  }
}, {
    // Other model options go here
    sequelize, // We need to pass the connection instance
    modelName: 'OrderSummary', // We need to choose the model name
    freezeTableName: true,
    timestamps: false,
});

module.exports = OrderSummary;