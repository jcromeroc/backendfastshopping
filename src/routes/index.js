const express = require('express');
const router = express.Router();

const shoppingController = require('../controllers/shoppingController');

module.exports = function() {
    // Rutas de la aplicacion
    router.get('/products', shoppingController.GetAllProducts);
    router.get('/products/:page/:order?', shoppingController.GetProductsPerPage);
    router.get('/customers/:email', shoppingController.GetCustomer);
    router.post('/customers', shoppingController.PostCustomer);
    router.post('/orders', shoppingController.PostOrder);
    router.put('/orders', shoppingController.UpdateOrder);
    router.post('/orders-summary', shoppingController.PostOrderSummary);
    return router;
};