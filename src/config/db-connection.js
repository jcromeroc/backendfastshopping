const { Sequelize } = require('sequelize');

// Change parameters to you local configuration ('database', 'username', 'password') and the host
module.exports = new Sequelize('shopping_cart', 'root', 'root', {
    host: 'localhost',
    dialect: 'mysql'
});