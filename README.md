Fast Shopping
===============



Instalación de la API
===============

1. Bajar el proyecto y en directorio raíz ejecutar el comando: npm install.
2. Modificar según la configuracion de Base de Datos el archivo: src/config/db-connection.js.
3. Ejecutar en el manejador de BD el script de mysql que se encuentra en la ruta: src/scripts/ShoppingCart.sql.
4. Si es necesario cambiar el puerto, modificar el archivo src/index.js en la línea 5; establecer el puerto que esté libre en su computadora. Recordar que si se hace este cambio aquí, también debe hacerlo en el frontend.



Uso del API
===============

Ejecutar en la raíz del proyecto el comando: npm run start.

Al hacer esto el servidor estará escuchando en el puerto designado.

Listo para usar el API!!!
